<?php

namespace Omnipay\AfterPay\Message;

use Symfony\Component\HttpFoundation\Response as HttpResponse;

class PurchaseResponse extends Response
{
    protected $script_sandbox = 'https://portal.sandbox.afterpay.com/afterpay.js';
    protected $script_live = 'https://portal.afterpay.com/afterpay.js';

    public function getRedirectMethod()
    {
        return 'POST';
    }

    public function getCountryCode()
    {
        return $this->getRequest()->getCard()->getBillingCountry();
    }
    /**
     * @return bool
     */
    public function isRedirect()
    {
        return !isset($this->getData()['errorCode']);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getRedirectResponse()
    {
        $output = <<<EOF
<html>
<head>
    <title>Redirecting...</title>
    <script src="%s" async></script>
</head>
<body>
    <script>
    function initAfterPay(){
        AfterPay.initialize({countryCode: "%s"});
        AfterPay.redirect({token: "%s"});
    }
    try{
        initAfterPay();
    }catch(e){
        setTimeout(function(){
            initAfterPay();
        },500);
    }
    </script>
</body>
</html>
EOF;

        $output = sprintf($output, $this->getScriptUrl(), $this->getCountryCode(), $this->getToken());

        return HttpResponse::create($output);
    }

    /**
     * @return string
     */
    public function getScriptUrl()
    {
        return $this->getRequest()->getTestMode() ? $this->script_sandbox : $this->script_live;
    }

    /**
     * @return string|null
     */
    public function getToken()
    {
        return isset($this->getData()['token']) ? $this->getData()['token'] : null;
    }

    /**
     * @return string
     */
    public function getTransactionReference()
    {
        return $this->getToken();
    }
}
